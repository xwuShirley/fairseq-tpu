#!/bin/bash
wget https://dl.fbaipublicfiles.com/fairseq/wav2vec/wav2vec_vox_new.pt

data_dir=$HOME/samples
model=$HOME/wav2vec_vox_new.pt
python /home/$user/fairseq/fairseq_cli/hydra_train.py \
    task.data=${data_dir} \
    model.w2v_path=${model} \
    --config-dir $HOME/fairseq-tpu \
    --config-name my_base


